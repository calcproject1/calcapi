const express = require("express");
const router = express.Router();
const userController = require("../controller/userControllers");
const { verify } = require("../config/jwt");

router.post("/create", userController.creatAccount);
router.post("/login", userController.login);
router.get("/details", verify, userController.userDetails);
router.post("/calculation", verify, userController.calculations);
router.get("/all/history", verify, userController.getAllhistory);
router.put("/select-history/:id", verify, userController.selectHistory);
router.put("/deselect-history/:id", verify, userController.deselectHistory);
router.put("/delete-history", verify, userController.deleteSelected);
router.put("/clear-history", verify, userController.clearHistory);
router.get("/", userController.allUser);

router.put("/update/name", verify, userController.updateName);
module.exports = router;
