const mongoose = require("mongoose");

const today = new Date();
const month = String(today.getMonth()).padStart(2, "0");
const date = String(today.getDate()).padStart(2, "0");
const year = String(today.getFullYear()).padStart(2, "0");
const formattedDate = `${month}/${date}/${year}`;

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  age: {
    type: Number,
    require: true,
  },
  email: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
  calcHistory: [
    {
      calcProblem: {
        type: String,
        require: true,
      },
      calcAnswer: {
        type: String,
        require: true,
      },
      isSelect: {
        type: Boolean,
        default: false,
      },
      timeStamp: {
        type: String,
        default: formattedDate,
      },
    },
  ],
});

const User = mongoose.model("User", userSchema);
module.exports = User;
