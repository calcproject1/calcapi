const express = require("express");
const cors = require("cors");
const db = require("./config/database");
const dotenv = require("dotenv");
const crypto = require("crypto");

// console.log(crypto.randomBytes(25).toString("hex"));

dotenv.config();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const userRouters = require("./route/userRoutes");
app.use("/", userRouters);

app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
