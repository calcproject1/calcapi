const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

const createAcessToken = async (user) => {
  const data = {
    Id: user.id,
    email: user.email,
  };
  return jwt.sign(data, process.env.JWT_SECRET);
};

const verify = async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    return res
      .status(404)
      .json({ status: false, message: "Authentication Required" });
  } else {
    token = token.slice(7);
    return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
      if (err) {
        res
          .status(401)
          .json({ status: false, message: "Unauthorized Authentication" });
      } else {
        req.user = data;
        next();
      }
    });
  }
};

const decodeToken = async (token) => {
  if (!token) return null;
  token = token.slice(7);
  return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
    if (err) return null;
    return data;
  });
};

module.exports = {
  createAcessToken,
  verify,
  decodeToken,
};
