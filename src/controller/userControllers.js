const User = require("../model/User");
const bcrypt = require("bcrypt");
const jwt = require("../config/jwt");

const creatAccount = async (req, res) => {
  try {
    const { name, email, age, password } = req.body;
    const checkEmail = await User.findOne({ email });
    const hashPassword = bcrypt.hashSync(password, 12);
    if (!name || !email || !password || !age) {
      return res
        .status(400)
        .json({ status: false, message: "Fields must not be Empty" });
    }
    if (checkEmail) {
      return res
        .status(409)
        .json({ status: false, message: "Email already Exist!" });
    }
    const user = new User({
      name,
      email,
      age,
      password: hashPassword,
    });
    await user.save();
    return res
      .status(201)
      .json({ status: true, message: "Successfully create Account" });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "Incorrect Email or Password" });
    }
    const checkPassword = bcrypt.compareSync(password, user.password);

    if (!checkPassword) {
      return res
        .status(404)
        .json({ status: false, message: "Incorrect Email or Password" });
    }
    const token = await jwt.createAcessToken(user);
    return res
      .status(200)
      .json({ status: false, message: "Succesfully Login", user, token });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

const userDetails = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    if (!token) {
      return res
        .status(401)
        .json({ status: false, message: "Unauthorized Action" });
    }
    const user = await User.findById(token.Id);
    if (!user) {
      return res.status(404).json({ status: false, message: "User not found" });
    }
    return res
      .status(200)
      .json({ status: true, message: `Hi! ${user.name}`, user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

// Calculation
const calculations = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    const { calcProblem, calcAnswer } = req.body;

    if (!calcAnswer || !calcProblem) {
      return res
        .status(400)
        .json({ status: false, message: "Fields must not be Empty" });
    }
    const user = await User.findById(token.Id);
    if (!user) {
      return res.status(404).json({ status: false, message: "User not found" });
    }
    user.calcHistory.push({
      calcProblem,
      calcAnswer,
    });
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "Successfully save calculation", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

const getAllhistory = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);

    const user = await User.findById(token.Id);
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "User or Calculation not found" });
    }
    const calcHistory = user.calcHistory;

    return res.status(200).json({
      status: true,
      message: "Here are all your history",
      calcHistory,
    });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};
const selectHistory = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    const id = req.params.id;
    const user = await User.findOne({
      $and: [{ _id: token.Id }, { "calcHistory._id": id }],
    });
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "User or Calculation not found" });
    }
    const calcHistory = user.calcHistory.find((calc) => calc.id === id);

    calcHistory.isSelect = true;
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "Calculation history selected", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};
const deselectHistory = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    const id = req.params.id;
    const user = await User.findOne({
      $and: [{ _id: token.Id }, { "calcHistory._id": id }],
    });
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "User or Calculation not found" });
    }
    const calcHistory = user.calcHistory.find((calc) => calc.id === id);

    calcHistory.isSelect = false;
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "Calculation history deselected", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};
const deleteSelected = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    const user = await User.findById(token.Id);
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "User or Calculation not found" });
    }
    const calcSelected = user.calcHistory.filter((calc) => !calc.isSelect);
    user.calcHistory = calcSelected;
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "Deleted History", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};
const clearHistory = async (req, res) => {
  try {
    const token = await jwt.decodeToken(req.headers.authorization);
    const user = await User.findById(token.Id);
    if (!user) {
      return res.status(404).json({ status: false, message: "User not found" });
    }
    user.calcHistory = [];
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "History Cleared", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};
const allUser = async (req, res) => {
  try {
    const user = await User.find();
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "Users not found" });
    }
    return res
      .status(200)
      .json({ status: true, message: "Here are all the Users", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

// Edit Profile

const updateName = async (req, res) => {
  try {
    const { name } = req.body;
    const token = await jwt.decodeToken(req.headers.authorization);
    const user = await User.findById(token.Id);
    if (!user) {
      return res
        .status(404)
        .json({ status: false, message: "Users not found" });
    }
    user.name = name;
    await user.save();
    return res
      .status(200)
      .json({ status: true, message: "Succesfully edit user name", user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retreive the user data" });
  }
};

module.exports = {
  creatAccount,
  login,
  userDetails,
  calculations,
  getAllhistory,
  selectHistory,
  deselectHistory,
  clearHistory,
  deleteSelected,
  allUser,
  updateName,
};
